# Homework 2

Simple Express-like app, but with promises. Provided by [Mihai Bojescu](https://github.com/MihaiBojescu).

## Building

First build the `lib` folder by entering it and running:
```sh
npm run build
```
Then enter the `app` folder and build it using:
```sh
npm run build
```

## Running

This can be done only after building. To run, go to the `app` folder, then use:
```sh
npm start 
```

## Debugging

To run in debug mode, use:
```sh
npm run debug
```
The intended way to debug is to attach to the debug process using the inspector provided by node. A sample debug config file can be seen in `.vscode/launch.json`.
