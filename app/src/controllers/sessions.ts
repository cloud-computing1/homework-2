import { isDeepStrictEqual } from 'util'
import { errors } from 'config'
import { Response, ServerError } from 'nodesque'
import { usersRepository } from 'repositories'
import { sessionsService, usersService } from 'services'
import { AuthRequest } from 'types'

export async function list (): Promise<void> {
  throw new ServerError(
    '[Sessions controller] User tried to list all sessions',
    405,
    errors.METHOD_NOT_ALLOWED
  )
}

export async function verify (req: AuthRequest): Promise<void> {
  const authorization = req.headers.authorization?.split(' ')?.[1]

  if (!authorization) {
    throw new ServerError(
      '[Sessions controller] User was not authenticated',
      401,
      errors.UNAUTHORIZED
    )
  }

  const session = await sessionsService.getInstance().getByToken(authorization)

  if (!session) {
    throw new ServerError(
      '[Sessions controller] Session for user not found',
      401,
      errors.UNAUTHORIZED
    )
  }

  const user = await usersRepository.getInstance().getByUserId(session.userId)

  if (!user) {
    throw new ServerError(
      '[Sessions controller] User for identified by session not found',
      401,
      errors.UNAUTHORIZED
    )
  }

  req.user = user
}

export async function edit (): Promise<void> {
  throw new ServerError(
    '[Sessions controller] User tried to list all sessions',
    405,
    errors.METHOD_NOT_ALLOWED
  )
}

export async function create (req: AuthRequest, res: Response) {
  const email = req.body.email as string | undefined
  const password = req.body.password as string | undefined

  if (!email || !password) {
    throw new ServerError(
      '[Sessions controller] User for identified by session not found',
      401,
      errors.UNAUTHORIZED
    )
  }

  const user = await usersService.getInstance().getByEmail(email)

  if (!user) {
    throw new ServerError(
      `[Sessions controller] User identified by ${email} was not found`,
      400,
      errors.BAD_PARAMETERS
    )
  }

  const session = await sessionsService.getInstance().create(user.userId)

  res.status(201).send(session)
}

export async function remove (req: AuthRequest, res: Response) {
  const authorization = req.headers.authorization?.split(' ')?.[1]
  const sessionId = req.params.sessionId as string | undefined

  if (!authorization || !sessionId) {
    throw new ServerError(
      '[Sessions controller] User was not authenticated',
      401,
      errors.UNAUTHORIZED
    )
  }

  const sessionByToken = await sessionsService.getInstance().getByToken(authorization)
  const sessionById = await sessionsService.getInstance().getById(sessionId)

  if (!sessionByToken) {
    throw new ServerError(
      `[Sessions controller] Session identified by token ${authorization} was not found`,
      404,
      { error: 'Session was not found' }
    )
  }

  if (!sessionById) {
    throw new ServerError(
      `[Sessions controller] Session identified by id ${sessionById} was not found`,
      404,
      { error: 'Session was not found' }
    )
  }

  if (!isDeepStrictEqual(sessionByToken, sessionById)) {
    throw new ServerError(
      `[Sessions controller] Session with id ${sessionById.token} can't be removed by an user with token ${authorization}`,
      403,
      { error: 'Forbidden' }
    )
  }

  await sessionsService.getInstance().remove(authorization)

  res.status(204).send()
}
