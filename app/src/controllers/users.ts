import { User } from 'entities'
import { usersService } from 'services'
import { AuthRequest } from 'types'
import { Response, ServerError } from 'nodesque'

export async function create (req: AuthRequest, res: Response): Promise<void> {
  const email = req.body.email as string | undefined
  const password = req.body.password as string | undefined

  if (!email || !password) {
    throw new ServerError(
      '[Users controller] No email or password provided',
      400,
      { error: 'Bad parameters' }
    )
  }

  const user = await usersService.getInstance().create(email, password)

  res.status(201).send(user)
}

export async function edit (req: AuthRequest, res: Response): Promise<void> {
  const user = req.user as User
  const userId = user.userId
  const email = req.body.email as string | undefined
  const password = req.body.password as string | undefined

  if (!email || !password) {
    throw new ServerError(
      '[Users controller] No email or password provided',
      400,
      { error: 'Bad parameters' }
    )
  }

  const updatedUser = await usersService.getInstance().edit(userId, email)

  res.status(200).send(updatedUser)
}

export async function remove (req: AuthRequest, res: Response): Promise<void> {
  const user = req.user as User
  const userId = user.userId

  if (!userId) {
    throw new ServerError(
      '[Users controller] No user id or content provided',
      400,
      { error: 'Bad parameters' }
    )
  }

  await usersService.getInstance().remove(userId)

  res.status(204).send()
}
