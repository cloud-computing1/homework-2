import { User } from 'entities'
import { articlesService } from 'services'
import { AuthRequest } from 'types'
import { Response, ServerError } from 'nodesque'
import { errors } from 'config'

export async function list (req: AuthRequest, res: Response): Promise<void> {
  const parsedSkip = Number(req.query.skip)
  const parsedTake = Number(req.query.take)
  const skip = !Number.isNaN(parsedSkip) && parsedSkip >= 0
    ? parsedSkip
    : 0
  const take = !Number.isNaN(parsedTake) && parsedTake >= 0
    ? parsedTake
    : 0

  const articles = await articlesService.getInstance().list(skip, take)

  res.status(200).send(articles)
}

export async function create (req: AuthRequest, res: Response): Promise<void> {
  const user = req.user as User
  const userId = user.userId
  const content = req.body.content as string | undefined

  if (!content) {
    throw new ServerError(
      '[Articles controller] No content provided',
      400,
      errors.BAD_PARAMETERS
    )
  }

  const article = await articlesService.getInstance().create(userId, content)

  res.status(201).send(article)
}

export async function edit (req: AuthRequest, res: Response): Promise<void> {
  const user = req.user as User
  const userId = user.userId
  const articleId = req.params.articleId as string | undefined
  const content = req.body.content as string | undefined

  if (!articleId || !content) {
    throw new ServerError(
      '[Articles controller] No article id or content provided',
      400,
      errors.BAD_PARAMETERS
    )
  }

  const article = await articlesService.getInstance().edit(articleId, userId, content)

  res.status(200).send(article)
}

export async function remove (req: AuthRequest, res: Response): Promise<void> {
  const user = req.user as User
  const userId = user.userId
  const articleId = req.params.articleId as string | undefined

  if (!articleId) {
    throw new ServerError(
      '[Articles controller] No article id or content provided',
      400,
      errors.BAD_PARAMETERS
    )
  }

  await articlesService.getInstance().remove(articleId, userId)

  res.status(204).send()
}
