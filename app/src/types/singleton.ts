export type Singleton = {
  [key: string]: (...args: any[]) => unknown | Promise<unknown>;
};

export type SingletonContainer<TSingleton extends Singleton> = {
  current: null | TSingleton;
};
