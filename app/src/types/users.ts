export type PublicUser = {
  userId: string;
  email: string;
};

export type CreateUserBody = {
  email?: string;
  password?: string;
};

export type UsersRouteBodies = CreateUserBody;

export type UsersRouteParams = {
  userId?: string
};
