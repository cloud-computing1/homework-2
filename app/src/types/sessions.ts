export type PublicSession = {
  id: string;
  userId: string;
  token: string;
};

export type CreateSessionBody = {
  email?: string;
  password?: string;
};

export type SessionsRoutesBodies = CreateSessionBody;

export type SessionsRoutesParams = {
  sessionId?: string;
};
