import { User } from 'entities'
import { Request } from 'nodesque'
import { ArticlesRouteBodies, ArticlesRouteParams } from './articles'
import { SessionsRoutesBodies, SessionsRoutesParams } from './sessions'
import { UsersRouteBodies, UsersRouteParams } from './users'

export type AuthRequest = Request & {
  user?: User;
  params?: UsersRouteParams | SessionsRoutesParams | ArticlesRouteParams;
  body?: UsersRouteBodies | SessionsRoutesBodies | ArticlesRouteBodies;
};
