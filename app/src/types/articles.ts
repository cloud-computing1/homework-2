export type CreateArticleBody = {
  content?: string;
};

export type EditArticleBody = {
  content?: string;
};

export type ArticlesRouteBodies = CreateArticleBody | EditArticleBody;

export type ArticlesRouteParams = {
  articleId?: string
}
