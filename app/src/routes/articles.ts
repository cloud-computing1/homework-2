import { articlesController, sessionsController } from 'controllers'
import { AuthRequest } from 'types'
import { Application, Response } from 'nodesque'

export function init (app: Application): void {
  app.get<AuthRequest, Response>('/articles', sessionsController.verify, articlesController.list)
  app.post<AuthRequest, Response>('/articles', sessionsController.verify, articlesController.create)
  app.patch<AuthRequest, Response>('/articles/:articleId', sessionsController.verify, articlesController.edit)
  app.delete<AuthRequest, Response>('/articles/:articleId', sessionsController.verify, articlesController.remove)
}
