import { usersController, sessionsController } from 'controllers'
import { AuthRequest } from 'types'
import { Application, Response } from 'nodesque'

export function init (app: Application): void {
  app.post<AuthRequest, Response>('/users', usersController.create)
  app.patch<AuthRequest, Response>('/users/:userId', sessionsController.verify, usersController.edit)
  app.delete<AuthRequest, Response>('/users/:userId', sessionsController.verify, usersController.remove)
}
