import { sessionsController } from 'controllers'
import { AuthRequest } from 'types'
import { Application, Response } from 'nodesque'

export function init (app: Application): void {
  app.get<AuthRequest, Response>('/sessions', sessionsController.list)
  app.post<AuthRequest, Response>('/sessions', sessionsController.create)
  app.put<AuthRequest, Response>('/sessions', sessionsController.edit)
  app.delete<AuthRequest, Response>('/sessions/:sessionId', sessionsController.remove)
}
