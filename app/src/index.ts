import { app } from './app'

app().then(app =>
  app
    .listen(3002)
    .and(() => console.log('Server up on port 3002'))
)
