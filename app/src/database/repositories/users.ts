
import { constants, errors } from 'config'
import { User } from 'entities'
import { ServerError } from 'nodesque'
import { getConnection } from 'typeorm'
import { Singleton, SingletonContainer } from 'types'
import { singleton } from 'utils'

export interface Repository extends Singleton {
  create: (email: string, password: string) => Promise<User>
  getByUserId: (userId: string) => Promise<User | undefined>
  getAllByEmail: (email: string) => Promise<User[]>
  getByEmail: (email: string) => Promise<User | undefined>
  getByEmailAndPassword: (email: string, password: string) => Promise<User | undefined>
  editByUserId: (userId: string, email?: string, password?: string) => Promise<void>
  removeByUserId: (userId: string) => Promise<void>
}

const container: SingletonContainer<Repository> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): Repository {
  const usersRepository = getConnection(constants.Database.name).getRepository(User)

  return {
    create,
    getByUserId,
    getAllByEmail,
    getByEmail,
    getByEmailAndPassword,
    editByUserId,
    removeByUserId
  }

  async function create (email: string, password: string): Promise<User> {
    try {
      return usersRepository.save({
        email,
        password,
        isDisabled: false
      })
    } catch (err) {
      throw new ServerError(
        '[Users repository] Could not create user with email and password',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function getByUserId (userId: string): Promise<User | undefined> {
    try {
      const user = await usersRepository.findOne({ userId })
      return user
    } catch (err) {
      throw new ServerError(
        '[Users repository] Could not get user by id',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function getAllByEmail (email: string): Promise<User[]> {
    try {
      const user = await usersRepository.find({ email })
      return user || []
    } catch (err) {
      throw new ServerError(
        '[Users repository] Could get all user by email',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function getByEmail (email: string): Promise<User | undefined> {
    try {
      const user = await getAllByEmail(email)
      return user[0]
    } catch (err) {
      throw new ServerError(
        '[Users repository] Could get user by email',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function getByEmailAndPassword (email: string, password: string): Promise<User | undefined> {
    try {
      const user = await usersRepository.findOne({ email, password })
      return user
    } catch (err) {
      throw new ServerError(
        '[Users repository] Could not get user by email and password',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function editByUserId (userId: string, email?: string, password?: string): Promise<void> {
    try {
      await usersRepository.update(userId, {
        ...(email && { email }),
        ...(password && { password })
      })
    } catch (err) {
      throw new ServerError(
        '[Users repository] Could not edit user by user id',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function removeByUserId (userId: string): Promise<void> {
    try {
      await usersRepository.update(userId, { isDisabled: true })
    } catch (err) {
      throw new ServerError(
        '[Users repository] Could not remove user by user id',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }
}
