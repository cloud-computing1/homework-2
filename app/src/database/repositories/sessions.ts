import { constants, errors } from 'config'
import { Session } from 'entities'
import { ServerError } from 'nodesque'
import { getConnection } from 'typeorm'
import { Singleton, SingletonContainer } from 'types'
import { singleton } from 'utils'

export interface Repository extends Singleton {
  create: (userId: string, token: string) => Promise<Session>;
  getById: (sessionId: string) => Promise<Session | undefined>;
  getByToken: (token: string) => Promise<Session | undefined>;
  removeByToken: (token: string) => Promise<void>;
  removeAllForUser: (userId: string) => Promise<void>;
}

const container: SingletonContainer<Repository> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): Repository {
  const sessionsRepository = getConnection(
    constants.Database.name
  ).getRepository(Session)

  return {
    create,
    getById,
    getByToken,
    removeByToken,
    removeAllForUser
  }

  async function create (userId: string, token: string): Promise<Session> {
    try {
      return sessionsRepository.save({
        userId,
        token
      })
    } catch (err) {
      throw new ServerError(
        '[Sessions repository] Could not create session for user',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function getById (sessionId: string): Promise<Session | undefined> {
    try {
      const session = await sessionsRepository.findOne({ id: sessionId })
      return session
    } catch (err) {
      console.error(err)
      throw new ServerError(
        '[Sessions repository] Could not get session by id',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function getByToken (token: string): Promise<Session | undefined> {
    try {
      const session = await sessionsRepository.findOne({ token })
      return session
    } catch (err) {
      throw new ServerError(
        '[Sessions repository] Could not get session by token',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function removeByToken (token: string): Promise<void> {
    try {
      await sessionsRepository.delete({ token })
    } catch (err) {
      throw new ServerError(
        '[Sessions repository] Could not remove session by token',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function removeAllForUser (userId: string): Promise<void> {
    try {
      await sessionsRepository.delete({ userId })
    } catch (err) {
      throw new ServerError(
        '[Sessions repository] Could not remove all session by user id',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }
}
