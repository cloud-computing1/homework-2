import { constants, errors } from 'config'
import { Article } from 'entities'
import { ServerError } from 'nodesque'
import { getConnection } from 'typeorm'
import { Singleton, SingletonContainer } from 'types'
import { singleton } from 'utils'

export interface Repository extends Singleton {
  create: (userId: string, content: string) => Promise<Article>;
  getById: (articleId: string) => Promise<Article | undefined>;
  list: (skip: number, take: number) => Promise<Article[]>;
  editByArticleId: (articleId: string, content: string) => Promise<void>;
  removeByArticleId: (articleId: string) => Promise<void>;
}

const container: SingletonContainer<Repository> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): Repository {
  const articlesRepository = getConnection(
    constants.Database.name
  ).getRepository(Article)

  return {
    create,
    getById,
    list,
    editByArticleId: edit,
    removeByArticleId: remove
  }

  async function create (userId: string, content: string): Promise<Article> {
    try {
      const article = await articlesRepository.save({
        userId,
        content
      })

      return article
    } catch (err) {
      throw new ServerError(
        '[Articles repository] Could not create article',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function getById (articleId: string): Promise<Article | undefined> {
    try {
      const article = await articlesRepository.findOne({ id: articleId })
      return article
    } catch (err) {
      throw new ServerError(
        '[Articles repository] Could not get article by id',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function list (skip: number, take: number): Promise<Article[]> {
    try {
      const articles = await articlesRepository.find({ skip, take })
      return articles || []
    } catch (err) {
      throw new ServerError(
        '[Articles repository] Could not get article by id',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function edit (
    articleId: string,
    content: string
  ): Promise<void> {
    try {
      await articlesRepository.update({ id: articleId }, { content: content })
    } catch (err) {
      throw new ServerError(
        '[Articles repository] Could not edit article by id',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }

  async function remove (articleId: string): Promise<void> {
    try {
      await articlesRepository.delete({ id: articleId })
    } catch (err) {
      throw new ServerError(
        '[Articles repository] Could not remove article by id',
        500,
        errors.INTERNAL_SERVER_ERROR
      )
    }
  }
}
