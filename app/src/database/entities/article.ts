import { Column, Entity, PrimaryColumn, Unique } from 'typeorm'
import { database } from 'utils'

@Entity('articles')
@Unique(['id'])
export class Article {
  @PrimaryColumn({
    type: 'uuid',
    generated: 'uuid',
    transformer: database.dashRemover
  })
  id!: string;

  @Column('uuid')
  userId!: string;

  @Column('text')
  content!: string;
}
