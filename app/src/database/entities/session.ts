import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  Unique
} from 'typeorm'
import { database } from 'utils'
import { User } from './user'

@Entity('sessions')
@Unique(['id', 'token'])
export class Session {
  @PrimaryColumn({
    type: 'uuid',
    generated: 'uuid',
    transformer: database.dashRemover
  })
  id!: string;

  @ManyToOne(() => User, (user) => user.userId, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE'
  })
  @JoinColumn({ name: 'userId' })
  @Column()
  userId!: string;

  @Column('char', { length: 64 })
  token!: string;
}
