import { Column, Entity, PrimaryColumn, Unique } from 'typeorm'
import { database } from 'utils'

@Entity('users')
@Unique(['userId', 'email'])
export class User {
  @PrimaryColumn({
    type: 'uuid',
    generated: 'uuid',
    transformer: database.dashRemover
  })
  userId!: string;

  @Column()
  email!: string;

  @Column()
  password!: string;

  @Column('bool')
  isDisabled!: boolean;
}
