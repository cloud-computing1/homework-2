import { Article } from 'entities'
import { isServerError, ServerError } from 'nodesque'
import { articlesRepository } from 'repositories'
import { Singleton, SingletonContainer } from 'types'
import { database, singleton } from 'utils'

export interface Service extends Singleton {
  list: (skip: number, take: number) => Promise<Article[]>;
  create: (userId: string, content: string) => Promise<Article>;
  edit: (
    articleId: string,
    userId: string,
    content: string
  ) => Promise<Article>;
  remove: (articleId: string, userId: string) => Promise<void>;
}

const container: SingletonContainer<Service> = { current: null }

export const getInstance = (
  articlesRepository?: articlesRepository.Repository
) =>
  singleton.create(container, () => {
    if (!articlesRepository) {
      throw new Error('[Articles service] Init error')
    }

    return constructor(articlesRepository)
  })

function constructor (
  articlesRepository: articlesRepository.Repository
): Service {
  return {
    list,
    create,
    edit,
    remove
  }

  async function list (skip: number, take: number): Promise<Article[]> {
    try {
      const articles = await articlesRepository.list(skip, take)

      return articles.map(map)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError('[Articles service] Could not list articles', 500, {
        error: 'Internal server error'
      })
    }
  }

  async function create (userId: string, content: string): Promise<Article> {
    try {
      const article = await articlesRepository.create(userId, content)

      return map(article)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError(
        '[Articles service] Could not create an article',
        500,
        { error: 'Internal server error' }
      )
    }
  }

  async function edit (
    articleId: string,
    userId: string,
    content: string
  ): Promise<Article> {
    try {
      const articleBeforeEdit = await articlesRepository.getById(articleId)

      if (!articleBeforeEdit) {
        throw new ServerError('[Articles service] Article not found,', 404, {
          error: 'Article not found'
        })
      }

      if (articleBeforeEdit.userId !== database.dashRemover.to(userId)) {
        throw new ServerError(
          `[Articles service] Article owned by ${articleBeforeEdit.userId} can't be edited by user with id ${userId}.`,
          403,
          { error: "Article can't be edited" }
        )
      }

      await articlesRepository.editByArticleId(articleId, content)
      const articleAfterEdit = await articlesRepository.getById(articleId)

      if (!articleAfterEdit) {
        throw new ServerError(
          `[Articles service] Article with id ${articleId} got deleted after update.`,
          500,
          { error: 'Internal server error' }
        )
      }

      return map(articleAfterEdit)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError(
        '[Articles service] Could not edit an article',
        500,
        { error: 'Internal server error' }
      )
    }
  }

  async function remove (articleId: string, userId: string): Promise<void> {
    try {
      const article = await articlesRepository.getById(articleId)

      if (!article) {
        throw new ServerError('[Articles service] Article not found,', 404, {
          error: 'Article not found'
        })
      }

      if (article.userId !== database.dashRemover.to(userId)) {
        throw new ServerError(
          `Article owned by ${article.userId} can't be removed by user with id ${userId}.`,
          403,
          { error: "Article can't be removed" }
        )
      }

      await articlesRepository.removeByArticleId(articleId)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError(
        '[Articles service] Could not remove session',
        500,
        { error: 'Internal server error' }
      )
    }
  }

  function map (article: Article): Article {
    return {
      ...article,
      id: database.dashRemover.from(article.id),
      userId: database.dashRemover.from(article.userId)
    }
  }
}
