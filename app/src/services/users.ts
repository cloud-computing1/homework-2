import { User } from 'entities'
import { isServerError, ServerError } from 'nodesque'
import { usersRepository } from 'repositories'
import { PublicUser, Singleton, SingletonContainer } from 'types'
import { database, singleton } from 'utils'

export interface Service extends Singleton {
  create: (email: string, password: string) => Promise<PublicUser>;
  getByUserId: (userId: string) => Promise<PublicUser | null>;
  getByEmail: (email: string) => Promise<PublicUser | null>;
  edit: (
    userId: string,
    email?: string,
    password?: string
  ) => Promise<PublicUser>;
  remove: (userId: string) => Promise<void>;
}

const container: SingletonContainer<Service> = { current: null }

export const getInstance = (usersRepository?: usersRepository.Repository) =>
  singleton.create(container, () => {
    if (!usersRepository) {
      throw new Error('[Users service] Init error')
    } else {
      return constructor(usersRepository)
    }
  })

function constructor (usersRepository: usersRepository.Repository): Service {
  return {
    create,
    getByUserId,
    getByEmail,
    edit,
    remove
  }

  async function create (email: string, password: string): Promise<PublicUser> {
    try {
      const usersWithSameEmail = await usersRepository.getAllByEmail(email)

      if (usersWithSameEmail.length > 0) {
        throw new ServerError(
          `[Users service] E-mail conflict for user with e-mail ${email} while attempting to create it`,
          409,
          { error: 'There are users that already use this e-mail' }
        )
      }

      const user = await usersRepository.create(email, password)

      return map(user)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError('[Users service] Failed to create user', 500, {
        error: 'Internal server error'
      })
    }
  }

  async function getByUserId (userId: string): Promise<PublicUser | null> {
    try {
      const user = await usersRepository.getByUserId(userId)

      if (!user) {
        return null
      }

      const publicUser: PublicUser = {
        userId: user.userId,
        email: user.email
      }

      return publicUser
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError('[Users service] Failed to create user', 500, {
        error: 'Internal server error'
      })
    }
  }

  async function getByEmail (email: string): Promise<PublicUser | null> {
    try {
      const user = await usersRepository.getByEmail(email)

      if (!user) {
        return null
      }

      return map(user)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError('[Users service] Failed to create user', 500, {
        error: 'Internal server error'
      })
    }
  }

  async function edit (
    userId: string,
    email?: string,
    password?: string
  ): Promise<PublicUser> {
    try {
      const userBeforeEdit = await usersRepository.getByUserId(userId)

      if (email) {
        const usersWithSameEmail = await usersRepository.getAllByEmail(email)

        if (usersWithSameEmail.length > 0) {
          throw new ServerError(
            `[Users service] E-mail conflict for user ${userId}`,
            409,
            { error: 'There are users that already use this e-mail' }
          )
        }
      }

      if (!userBeforeEdit) {
        throw new ServerError(
          `[Users service] Could not find user with user id ${userId}`,
          404,
          { error: `User with id ${userId} not found` }
        )
      }

      await usersRepository.editByUserId(userId, email, password)
      const userAfterEdit = await usersRepository.getByUserId(userId)

      if (!userAfterEdit) {
        throw new ServerError(
          `[Users service] User with id ${userId} got deleted after update.`,
          500,
          { error: 'Internal server error' }
        )
      }

      return map(userAfterEdit)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError('[Users service] Failed to create user', 500, {
        error: 'Internal server error'
      })
    }
  }

  async function remove (userId: string): Promise<void> {
    try {
      const user = await usersRepository.getByUserId(userId)

      if (!user) {
        throw new ServerError(
          `[Users service] Could not find user with user id ${userId} for removal`,
          404,
          { error: `User with id ${userId} not found` }
        )
      }

      await usersRepository.removeByUserId(userId)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError('[Users service] Failed to create user', 500, {
        error: 'Internal server error'
      })
    }
  }

  function map (user: User): PublicUser {
    return {
      ...user,
      userId: database.dashRemover.from(user.userId)
    }
  }
}
