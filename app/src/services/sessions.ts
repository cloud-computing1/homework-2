import { Session } from 'entities'
import { isServerError, ServerError } from 'nodesque'
import { sessionsRepository } from 'repositories'
import { cryptoService } from 'services'
import { PublicSession, Singleton, SingletonContainer } from 'types'
import { database, singleton } from 'utils'

export interface Service extends Singleton {
  create: (userId: string) => Promise<PublicSession>;
  getById: (sessionId: string) => Promise<PublicSession | null>;
  getByToken: (token: string) => Promise<PublicSession | null>;
  remove: (token: string) => Promise<void>;
}

const container: SingletonContainer<Service> = { current: null }

export const getInstance = (
  sessionsRepository?: sessionsRepository.Repository,
  cryptoService?: cryptoService.Service
) =>
  singleton.create(container, () => {
    if (!sessionsRepository || !cryptoService) {
      throw new Error('[Sessions service] Init error')
    } else {
      return constructor(sessionsRepository, cryptoService)
    }
  })

function constructor (
  sessionsRepository: sessionsRepository.Repository,
  cryptoService: cryptoService.Service
): Service {
  return {
    create,
    getById,
    getByToken,
    remove
  }

  async function create (userId: string): Promise<PublicSession> {
    try {
      const token = await cryptoService.randomStringHex(24)
      const session = await sessionsRepository.create(userId, token)

      const publicSession: PublicSession = {
        id: session.id,
        userId: session.userId,
        token: session.token
      }

      return publicSession
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError(
        '[Sessions service] Could not create a session',
        500,
        { error: 'Internal server error' }
      )
    }
  }

  async function getById (sessionId: string): Promise<PublicSession | null> {
    try {
      const session = await sessionsRepository.getById(sessionId)

      if (!session) {
        return null
      }

      return map(session)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError(
        '[Sessions service] Could not get session by token',
        500,
        { error: 'Internal server error' }
      )
    }
  }

  async function getByToken (token: string): Promise<PublicSession | null> {
    try {
      const session = await sessionsRepository.getByToken(token)

      if (!session) {
        return null
      }

      return map(session)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError(
        '[Sessions service] Could not get session by token',
        500,
        { error: 'Internal server error' }
      )
    }
  }

  async function remove (token: string): Promise<void> {
    try {
      const session = await sessionsRepository.getByToken(token)

      if (!session) {
        throw new ServerError(
          `[Sessions service] Session identified by token ${token} not found for removal`,
          404,
          { error: 'Article not found' }
        )
      }

      await sessionsRepository.removeByToken(token)
    } catch (err) {
      if (isServerError(err)) {
        throw err
      }

      throw new ServerError(
        '[Sessions service] Could not remove session by token',
        500,
        { error: 'Internal server error' }
      )
    }
  }

  function map (session: Session): PublicSession {
    return {
      ...session,
      id: database.dashRemover.from(session.id),
      userId: database.dashRemover.from(session.userId)
    }
  }
}
