import crypto from 'crypto'
import { Singleton, SingletonContainer } from 'types'
import util from 'util'
import { singleton } from 'utils'
import { ServerError } from 'nodesque'

export interface Service extends Singleton {
  randomStringHex: (size: number) => Promise<string>
  randomStringBase64: (size: number) => Promise<string>
  hash: (text: string) => string
}

const container: SingletonContainer<Service> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): Service {
  const hashAlgorithm = 'whirlpool'
  const randomBytes = util.promisify(crypto.randomBytes)

  return {
    randomStringHex,
    randomStringBase64,
    hash
  }

  async function randomStringHex (size: number): Promise<string> {
    try {
      const bytes = await randomBytes(size)
      const string = bytes.toString('hex')

      return string
    } catch (err) {
      throw new ServerError(
        '[Crypto service] Could not create a random hex string',
        500,
        { error: 'Internal server error' }
      )
    }
  }

  async function randomStringBase64 (size: number): Promise<string> {
    try {
      const bytes = await randomBytes(size)
      const string = bytes.toString('base64')

      return string
    } catch (err) {
      throw new ServerError(
        '[Crypto service] Could not create a random base64 string',
        500,
        { error: 'Internal server error' }
      )
    }
  }

  function hash (text: string): string {
    try {
      return crypto
        .createHash(hashAlgorithm)
        .update(text)
        .digest('base64')
    } catch (err) {
      throw new ServerError(
        '[Crypto service] Could not hash a string',
        500,
        { error: 'Internal server error' }
      )
    }
  }
}
