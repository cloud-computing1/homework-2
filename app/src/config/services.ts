import {
  articlesRepository,
  sessionsRepository,
  usersRepository
} from 'repositories'
import {
  articlesService,
  cryptoService,
  sessionsService,
  usersService
} from 'services'

export function init () {
  const usersRepo = usersRepository.getInstance()
  const sessionsRepo = sessionsRepository.getInstance()
  const articlesRepo = articlesRepository.getInstance()

  const crypto = cryptoService.getInstance()
  usersService.getInstance(usersRepo)
  sessionsService.getInstance(sessionsRepo, crypto)
  articlesService.getInstance(articlesRepo)
}
