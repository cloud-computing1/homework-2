export const METHOD_NOT_ALLOWED = { error: 'Method not allowed' }
export const BAD_PARAMETERS = { error: 'Bad parameters' }
export const UNAUTHORIZED = { error: 'Unauthorized' }
export const INTERNAL_SERVER_ERROR = { error: 'Internal server error' }
