import process from 'process'
import { Article, Session, User } from 'entities'
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'

export const PORT = Number(process.env.PORT || 3000)

export const Database: PostgresConnectionOptions = {
  name: 'postgres',
  type: 'postgres',
  host: process.env.DATABASE_HOST,
  port: Number(process.env.DATABASE_PORT || 5432),
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: 'hw2',
  entities: [
    User,
    Session,
    Article
  ]
}
