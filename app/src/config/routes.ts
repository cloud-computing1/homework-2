import { Application } from 'nodesque'
import { articlesRoutes, sessionsRoutes, usersRoutes } from 'routes'

export function init (app: Application): void {
  usersRoutes.init(app)
  sessionsRoutes.init(app)
  articlesRoutes.init(app)
}
