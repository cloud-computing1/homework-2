import { constants } from 'config'
import { createConnection } from 'typeorm'

export async function init () {
  try {
    const connection = await createConnection(constants.Database)

    await connection.synchronize()
  } catch (err) {
    console.error(err)
    throw new Error('[Database] Could not create database connection')
  }
}
