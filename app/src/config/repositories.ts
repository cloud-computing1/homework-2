import {
  articlesRepository,
  sessionsRepository,
  usersRepository
} from 'repositories'

export function init () {
  usersRepository.getInstance()
  sessionsRepository.getInstance()
  articlesRepository.getInstance()
}
