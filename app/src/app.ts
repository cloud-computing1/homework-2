import { Application, Nodesque } from 'nodesque'
import { database, repositories, routes, services } from 'config'

export async function app (): Promise<Application> {
  await database.init()
  repositories.init()
  services.init()

  const nodesque = Nodesque()

  routes.init(nodesque)

  return nodesque
}
