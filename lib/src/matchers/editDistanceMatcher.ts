import { Request, Response } from 'middleware'
import { Path } from 'router'
import { Matcher } from './types'

export function EditDistanceMatcher (): Matcher {
  return {
    match
  }

  function match<TRequest extends Request, TResponse extends Response> (
    paths: Path<TRequest, TResponse>,
    url: string
  ): string | undefined {
    const matches = Object.keys(paths)
      .map((path) => [path, editDistance(path, url)] as [string, number])
      .sort(
        ([_1, editDistanceA], [_2, editDistanceB]) =>
          editDistanceA - editDistanceB
      )

    return matches[0]?.[0]
  }

  function editDistance (stringA: string, stringB: string): number {
    return memoizedEditDistance(stringA.length - 1, stringB.length - 1)

    function memoizedEditDistance (indexA: number, indexB: number): number {
      if (indexA === 0) {
        return indexB
      }

      if (indexB === 0) {
        return indexA
      }

      if (stringA[indexA - 1] === stringB[indexB]) {
        return memoizedEditDistance(indexA - 1, indexB - 1)
      }

      return (
        1 +
        min(
          memoizedEditDistance(indexA, indexB - 1),
          memoizedEditDistance(indexA - 1, indexB),
          memoizedEditDistance(indexA - 1, indexB - 1)
        )
      )
    }
  }

  function min (a: number, b: number, c: number): number {
    if (a < b && a < c) {
      return a
    } else if (b < a && b < c) {
      return b
    } else {
      return c
    }
  }
}
