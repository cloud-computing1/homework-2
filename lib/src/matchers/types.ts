import { Request, Response } from 'middleware'
import { Path } from 'router'

export interface Matcher {
  match: <TRequest extends Request, TResponse extends Response>(
    paths: Path<TRequest, TResponse>,
    url: string
  ) => keyof Path<TRequest, TResponse> | undefined;
}
