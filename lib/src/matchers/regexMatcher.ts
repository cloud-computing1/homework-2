import { constants, regex } from 'config'
import { error } from 'errors'
import { Request, Response } from 'middleware'
import { Path } from 'router'
import { Matcher } from './types'

export function RegexMatcher (): Matcher {
  return {
    match
  }

  function match<TRequest extends Request, TResponse extends Response> (
    paths: Path<TRequest, TResponse>,
    url: string
  ): string | undefined {
    if (!paths) {
      throw error('0x0000')(url)
    }

    const urlWithoutQuery = url.split(constants.QUERY_DELIMITER)?.[0] as string
    const match = Object.keys(paths)
      .map((path) => [path, wildcardPath(path)] as [string, RegExp])
      .find(([_1, regex]) => regex.test(urlWithoutQuery))

    return match?.[0]
  }

  function wildcardPath (path: string): RegExp {
    const abstractPath = path.replace(regex.WILDCARD_REGEX, '/.*?')
    return RegExp(abstractPath)
  }
}
