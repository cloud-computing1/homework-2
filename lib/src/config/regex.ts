export const PATH_REGEX = /(\/[\w\d\-_]*)+/i
export const WILDCARD_REGEX = /\/(:[\w\d\-_]+)/g
