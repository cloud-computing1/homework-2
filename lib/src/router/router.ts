import { error } from 'errors'
import { Matcher } from 'matchers'
import { Middleware, Request, Response } from 'middleware'
import { ExternalRouter, InternalRouter, Routes, Verb } from './types'
import { validator } from './validator'

export function Router (matcher: Matcher): InternalRouter {
  const routes: Routes = {
    [Verb.GET]: {},
    [Verb.POST]: {},
    [Verb.PUT]: {},
    [Verb.PATCH]: {},
    [Verb.DELETE]: {}
  }

  const self: InternalRouter = {
    get,
    post,
    put,
    patch,
    delete: remove,
    route,
    getExternalRouter
  }

  return self

  function get<TRequest extends Request, TResponse extends Response> (
    path: string,
    ...middlewares: Middleware<TRequest, TResponse>[]
  ): void {
    append<TRequest, TResponse>(Verb.GET, path, middlewares)
  }

  function post<TRequest extends Request, TResponse extends Response> (
    path: string,
    ...middlewares: Middleware<TRequest, TResponse>[]
  ): void {
    append<TRequest, TResponse>(Verb.POST, path, middlewares)
  }

  function put<TRequest extends Request, TResponse extends Response> (
    path: string,
    ...middlewares: Middleware<TRequest, TResponse>[]
  ): void {
    append<TRequest, TResponse>(Verb.PUT, path, middlewares)
  }

  function patch<TRequest extends Request, TResponse extends Response> (
    path: string,
    ...middlewares: Middleware<TRequest, TResponse>[]
  ): void {
    append<TRequest, TResponse>(Verb.PATCH, path, middlewares)
  }

  function remove<TRequest extends Request, TResponse extends Response> (
    path: string,
    ...middlewares: Middleware<TRequest, TResponse>[]
  ): void {
    append<TRequest, TResponse>(Verb.DELETE, path, middlewares)
  }

  function route<TRequest extends Request, TResponse extends Response> (
    method: string | undefined,
    path: string | undefined
  ): [string, Middleware<TRequest, TResponse>[]] {
    if (!method || !path) {
      throw error('0x0003')(method, path)
    }

    if (!(method in Verb)) {
      throw error('0x0002')(method, path)
    }

    const possiblePaths = routes[method as Verb]
    const matchedPath = matcher.match(possiblePaths, path)

    if (!matchedPath) {
      throw error('0x0002')(method, path)
    }

    const matchedMiddlewares = routes[method as Verb][matchedPath]

    if (!matchedMiddlewares) {
      throw error('0x0002')(method, path)
    }

    return [matchedPath as string, matchedMiddlewares]
  }

  function getExternalRouter (): ExternalRouter {
    return {
      get: self.get,
      post: self.post,
      put: self.put,
      patch: self.patch,
      delete: self.delete
    }
  }

  function append<TRequest extends Request, TResponse extends Response> (
    method: Verb,
    path: string,
    middlewares: Middleware<TRequest, TResponse>[]
  ): void {
    if (!validator(path)) {
      throw error('0x0000')(path)
    }

    if (!routes[method]) {
      routes[method] = {}
    }

    if (!routes[method][path]) {
      routes[method][path] = Object.assign([], middlewares)
      return
    }

    throw error('0x0001')(method, path)
  }
}
