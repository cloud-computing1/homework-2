import { ExternalRouter } from 'router'

export type AndCallback = (callback: () => void) => void;

export type Application = ExternalRouter & {
  listen: (
    port: number
  ) => {
    and: AndCallback;
  };
};
