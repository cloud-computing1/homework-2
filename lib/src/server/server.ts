import {
  BasicRequestDecorator,
  BasicResponseDecorator,
  BasicServerDecorator
} from 'decorators'
import { error, isServerError } from 'errors'
import http, { IncomingMessage, ServerOptions, ServerResponse } from 'http'
import { RegexMatcher } from 'matchers'
import { Router } from 'router'
import { InternalRouter } from 'router/types'
import { Application } from './types'

export function Nodesque (config?: ServerOptions): Application {
  const options = config ?? {}
  const matcher = RegexMatcher()
  const router = Router(matcher)

  const server = http.createServer(options, process(router))
  const app = BasicServerDecorator(router.getExternalRouter()).decorate(server)

  return app
}

function process (router: InternalRouter) {
  return async (req: IncomingMessage, res: ServerResponse): Promise<void> => {
    try {
      const [route, middlewares] = router.route(
        req.method?.toUpperCase(),
        req.url
      )

      const transformedReq = await BasicRequestDecorator(route).decorate(req)
      const transformedRes = await BasicResponseDecorator().decorate(res)

      await middlewares.reduce(
        async (previousMiddleware, middleware) => {
          await previousMiddleware

          if (!transformedRes.done) {
            return middleware(transformedReq, transformedRes)
          }
        },
        new Promise<void>((resolve) => resolve())
      )

      if (!transformedRes.done) {
        throw error('0x0004')(req.method?.toUpperCase(), req.url)
      }
    } catch (err) {
      const transformedRes = await BasicResponseDecorator().decorate(res)

      console.error(err.message)

      if (isServerError(err)) {
        transformedRes.status(err.status).send(err.body)
      } else {
        transformedRes.status(500).send({ error: 'Internal server error' })
      }
    }
  }
}
