import { ServerError } from './serverError'
import { ErrorType, ErrorValue } from './types'
import { errors } from './values'

export function error (type: ErrorType) {
  const constructor = errors[type]

  return build(constructor)
}

function build<T extends (...args: any[]) => any>(
  constructor: T
): (...args: Parameters<T>) => ServerError {
  return (...args) => {
    const results = constructor(...args) as ErrorValue
    return new ServerError(results.reason, results.status, results.body)
  }
}
