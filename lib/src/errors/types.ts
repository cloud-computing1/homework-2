import { errors } from './values'

export type ErrorMessage = {
  error: string;
};

export type ErrorValue = {
  reason: string;
  status: number;
  body: ErrorMessage;
};

export type ErrorType = keyof typeof errors;
