import { ServerResponse } from 'http'
import { Response } from 'middleware'
import { ResponseDecorator } from './types'

export function BasicResponseDecorator (): ResponseDecorator {
  let shouldSkip = false

  return {
    decorate
  }

  async function decorate (res: ServerResponse | Response): Promise<Response> {
    const transformedRes = {
      ...res,
      done: false,
      send: sendFunction(res),
      status: statusFunction(res),
      finishWith: skipRouters(res, true),
      withoutFinishing: skipRouters(res, false)
    } as Response

    return transformedRes

    function sendFunction (res: ServerResponse | Response): Response['send'] {
      return (body) => {
        res.setHeader('Content-Type', 'application/json')
        res.setHeader('Server', 'Nodesque')
        res.setHeader('Date', new Date().toISOString())

        if (!body) {
          res.end()
        } else {
          res.write(JSON.stringify(body))
          res.end()
        }

        if (!shouldSkip) {
          transformedRes.done = true
        }
      }
    }

    function statusFunction (
      res: ServerResponse | Response
    ): Response['status'] {
      return (status) => {
        res.statusCode = status

        return {
          send: sendFunction(res)
        }
      }
    }

    function skipRouters (
      res: ServerResponse | Response,
      skip: boolean
    ): Response['finishWith'] | Response['withoutFinishing'] {
      return () => {
        shouldSkip = skip

        return {
          status: statusFunction(res)
        }
      }
    }
  }
}
