import { IncomingMessage, Server, ServerResponse } from 'http'
import { Request, Response } from 'middleware'
import { Application } from 'server'

export interface RequestDecorator {
  decorate: (req: IncomingMessage | Request) => Promise<Request>;
}

export interface ResponseDecorator {
  decorate: (req: ServerResponse | Response) => Promise<Response>;
}

export interface ServerDecorator {
  decorate: (req: Server) => Application;
}
