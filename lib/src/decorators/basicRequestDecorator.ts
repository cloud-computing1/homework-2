import { constants } from 'config'
import { error } from 'errors'
import { IncomingMessage } from 'http'
import { Request } from 'middleware'
import { RequestDecorator } from './types'

export function BasicRequestDecorator (route: string): RequestDecorator {
  return {
    decorate
  }

  async function decorate (req: IncomingMessage | Request): Promise<Request> {
    return {
      ...req,
      headers: req.headers,
      params: parseParams(req),
      query: parseQuery(req),
      body: await parseBody(req)
    } as Request
  }

  function parseParams (req: IncomingMessage | Request): Record<string, string> {
    if (!req.url) {
      throw error('0x0000')('')
    }

    try {
      const urlWithoutQuery = req.url.split(
        constants.QUERY_DELIMITER
      )?.[0] as string

      const routeSegments = route.split(constants.PATH_DELIMITER)
      const urlSegments = urlWithoutQuery.split(constants.PATH_DELIMITER)

      const params = urlSegments.reduce<Record<string, string>>(
        (acc, segment, index) => {
          const routeSegment = routeSegments[index]

          if (routeSegment?.charAt(0) === ':') {
            acc[routeSegment.substr(1)] = segment
          }

          return acc
        },
        {}
      )

      return params
    } catch (err) {
      throw error('0x0005')(req.method?.toUpperCase(), req.url)
    }
  }

  function parseQuery (req: IncomingMessage | Request): Record<string, string> {
    if (!req.url) {
      throw error('0x0000')('')
    }

    try {
      const unparsedParams = req.url.split(constants.QUERY_DELIMITER)?.[1]
      const splittedParams = unparsedParams?.split(
        constants.QUERY_AND_DELIMITER
      )

      if (!splittedParams) {
        return {}
      }

      const query = splittedParams.reduce((query, param) => {
        const [key, value] = param.split('=') as [string, string]
        query[key] = value

        return query
      }, {} as Record<string, string>)

      return query
    } catch (err) {
      throw error('0x0006')(req.method?.toUpperCase(), req.url)
    }
  }

  async function parseBody (
    req: IncomingMessage | Request
  ): Promise<Record<string, string>> {
    try {
      const rawData = await new Promise<string>((resolve, reject) => {
        let data = ''

        req.on('data', (chunk) => (data += chunk))
        req.on('end', () => resolve(data))
        req.on('err', (err) => reject(err))
      })

      if (!rawData) {
        return {}
      }

      return JSON.parse(rawData)
    } catch (err) {
      throw error('0x0007')(req.method, req.url, err)
    }
  }
}
